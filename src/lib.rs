// This is needed due to a bug with clippy generating a warning
// when using #[wasm_bindgen]
// Info here: https://github.com/rustwasm/wasm-bindgen/issues/2774
#![allow(clippy::unused_unit)]

mod utils;

extern crate base64;
extern crate image;
use crate::utils::set_panic_hook;
use base64::encode;
use image::{DynamicImage, GenericImage, ImageFormat, Pixel};
use num_traits::{clamp, NumCast};
use std::io::{Cursor, Read, Seek, SeekFrom};
use std::panic;
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
extern "C" {
    // Use `js_namespace` here to bind `console.log(..)` instead of `log(..)`
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

#[wasm_bindgen]
/// Function exposed to the JS side as the library entry point
pub fn transfer_color(_target_arr: &[u8], _src_arr: &[u8]) -> Result<(), JsValue> {
    set_panic_hook();
    let mut target_img = load_image_from_array(_target_arr);
    let mut src_img = load_image_from_array(_src_arr);
    color_transfer(&mut target_img, &mut src_img);

    // convert image to base64 to be displayed
    let base64_str = convert_to_base64(src_img);
    // append it as an img element to the html
    append_img_to_dom(base64_str)
}

/// Compute the mean of each channel in the image
fn compute_channel_means<I: GenericImage>(img: &mut I) -> Vec<f32> {
    let mut r_mean: f32 = 0.0;
    let mut g_mean: f32 = 0.0;
    let mut b_mean: f32 = 0.0;

    let (width, height) = img.dimensions();
    for x in 0..width {
        for y in 0..height {
            let pixel = img.get_pixel(x, y);
            // get the channels from the pixel
            #[allow(deprecated)]
            let (r, g, b, _a) = pixel.channels4();
            let vec: (f32, f32, f32) = (
                NumCast::from(r).unwrap(),
                NumCast::from(g).unwrap(),
                NumCast::from(b).unwrap(),
            );
            r_mean += vec.0;
            g_mean += vec.1;
            b_mean += vec.2;
        }
    }

    // return the mean of each channel
    vec![
        r_mean / (width * height) as f32,
        g_mean / (width * height) as f32,
        b_mean / (width * height) as f32,
    ]
}

/// Compute the standard deviation of each channel in the image
fn compute_channel_stddevs<I: GenericImage>(img: &mut I, means: &[f32]) -> Vec<f32> {
    let mut r_stdev: f32 = 0.0;
    let mut g_stdev: f32 = 0.0;
    let mut b_stdev: f32 = 0.0;

    let (width, height) = img.dimensions();
    for x in 0..width {
        for y in 0..height {
            let pixel = img.get_pixel(x, y);
            // get the channels from the pixel
            #[allow(deprecated)]
            let (r, g, b, _a) = pixel.channels4();
            let channels: (f32, f32, f32) = (
                NumCast::from(r).unwrap(),
                NumCast::from(g).unwrap(),
                NumCast::from(b).unwrap(),
            );
            r_stdev += (channels.0 - means[0]).powi(2);
            g_stdev += (channels.1 - means[1]).powi(2);
            b_stdev += (channels.2 - means[2]).powi(2);
        }
    }

    // return all the stdevs as a vector
    vec![
        r_stdev.sqrt() / (width * height) as f32,
        g_stdev.sqrt() / (width * height) as f32,
        b_stdev.sqrt() / (width * height) as f32,
    ]
}

/// Perform the color transfer from target img to source img
pub fn color_transfer<I: GenericImage>(img_target: &mut I, img_source: &mut I) {
    // get the means of each channel for both images
    let target_means = compute_channel_means(img_target);
    let source_means = compute_channel_means(img_source);

    // get the standard deviations of each channel for both images
    let mut target_stdevs = compute_channel_stddevs(img_target, &target_means);
    let mut source_stdevs = compute_channel_stddevs(img_source, &source_means);

    // make sure the stdevs are not zero to avoid division by zero
    for i in 0..3 {
        if target_stdevs[i] == 0.0 {
            target_stdevs[i] += 0.00001;
        }
        if source_stdevs[i] == 0.0 {
            source_stdevs[i] = 0.00001;
        }
    }

    // replace pixels in img_source with the new values
    let (width, height) = img_source.dimensions();
    for x in 0..width {
        for y in 0..height {
            let pixel = img_source.get_pixel(x, y);
            // get the channels from the pixel
            #[allow(deprecated)]
            let (r, g, b, a) = pixel.channels4();
            let channels: (f32, f32, f32) = (
                NumCast::from(r).unwrap(),
                NumCast::from(g).unwrap(),
                NumCast::from(b).unwrap(),
            );
            // compute the new values for each channel
            let new_r = (channels.0 - source_means[0]) * target_stdevs[0] / source_stdevs[0]
                + target_means[0];
            let new_g = (channels.1 - source_means[1]) * target_stdevs[1] / source_stdevs[1]
                + target_means[1];
            let new_b = (channels.2 - source_means[2]) * target_stdevs[2] / source_stdevs[2]
                + target_means[2];
            let max = 255f32;

            // set the new pixel with the new values
            #[allow(deprecated)]
            let new_pixel = Pixel::from_channels(
                NumCast::from(clamp(new_r, 0.0, max)).unwrap(),
                NumCast::from(clamp(new_g, 0.0, max)).unwrap(),
                NumCast::from(clamp(new_b, 0.0, max)).unwrap(),
                a,
            );
            img_source.put_pixel(x, y, new_pixel);
        }
    }
}

/// Retrieve the image data as an array of u8's and convert it to a DynamicImage
fn load_image_from_array(_array: &[u8]) -> DynamicImage {
    let img = match image::load_from_memory_with_format(_array, ImageFormat::Png) {
        Ok(img) => img,
        Err(error) => {
            panic!("{:?}", error)
        }
    };

    img
}

/// Convert the image data to a format that can be understood and displayed by the browser
fn convert_to_base64(img: DynamicImage) -> String {
    // Create a some memory space to store the image
    let mut cursor = Cursor::new(Vec::new());
    match img.write_to(&mut cursor, ImageFormat::Png) {
        Ok(c) => c,
        Err(error) => {
            panic!("Error writing to buffer: {:?}", error)
        }
    };

    cursor.seek(SeekFrom::Start(0)).unwrap();
    let mut out = Vec::new();
    // Read data from memory
    cursor.read_to_end(&mut out).unwrap();
    // Decoding
    let base64_repr = encode(&mut out);
    let str_output = format!("{}{}", "data:image/png;base64,", base64_repr);
    str_output
}

/// Append the result to the DOM in a src image element
pub fn append_img_to_dom(image_src: String) -> Result<(), JsValue> {
    let window = web_sys::window().expect("window doesn't exist");
    let document = window.document().expect("document doesn't exist");
    // get the result-container section of the Dom
    let result_container = document
        .get_element_by_id("result-container")
        .expect("Result container should exist");
    let img = document.create_element("img")?;

    img.set_attribute("src", &image_src)?;
    img.set_attribute("id", "result-image")?;

    result_container.append_child(&img)?;

    Ok(())
}

#[test]
fn test_compute_channel_means() {
    let mut img = load_image_from_array(&include_bytes!("../images/source1.png")[..]);
    let means = compute_channel_means(&mut img);
    assert_eq!(means, vec![148.38457, 148.46497, 144.8339]);

    let mut img_2 = load_image_from_array(&include_bytes!("../images/source2.png")[..]);
    let means_2 = compute_channel_means(&mut img_2);
    assert_eq!(means_2, vec![126.68959, 141.05054, 117.92196]);
}

#[test]
fn test_comput_std_devs() {
    let mut img = load_image_from_array(&include_bytes!("../images/source1.png")[..]);
    let means = compute_channel_means(&mut img);
    let stdevs = compute_channel_stddevs(&mut img, &means);
    assert_eq!(stdevs, vec![0.072354496, 0.06436253, 0.052418806]);

    let mut img_2 = load_image_from_array(&include_bytes!("../images/source2.png")[..]);
    let means_2 = compute_channel_means(&mut img_2);
    let stdevs_2 = compute_channel_stddevs(&mut img_2, &means_2);
    assert_eq!(stdevs_2, vec![0.085278906, 0.07931944, 0.11645323]);
}
