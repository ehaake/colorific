<div align="center">

  <h1><code>Colorific!</code></h1>

  <strong>A kinda cool and very basic web-app for transferring the color between two images</strong>

  <sub>Built with 🦀🕸 and other things too</sub>
</div>

## 📚 About
Like an image but don't like the colors? Correct it in a couple of clicks! Just find an image whose colors you like and transfer them to your image simply and easily! 

_Quality of results depend on choice of source and target. Don't blame me for your poor taste_ 🙂 _But feel free to experiment!_

## **IMPORTANT!** Currently only works with png files

## 🚴 Usage

### 🛠️ Build with `wasm-pack build`
Whenever changes are made to the Rust source, make sure to rebuild.

```
wasm-pack build
```

### 🛠️ Run the local web service with `npm run start`
To test the app, navigate to the www/ folder and run

```
cd www/
npm run start
```

Then navigate to `http://localhost:8080/` in your favorite browser
<br><br>

# ✏️ Report
## What was Built
This is a *very* simple static single page web app that transfers the colors from a target image to a source image using a basic statistical analysis from [this paper](http://www.cs.tau.ac.il/~turkel/imagepapers/ColorTransfer.pdf).

The inspiration for this project was my desire to learn about incorporating web assembly into web applications built using traditional web programming tools such as javascript. Doing the image computation on the client side is usually a better choice for interactive applications due to removing the overhead of transferring the data to a remote server for computationa and then back again. However, javascript is often not a good choice for performance sensitive applications, hence the desire to learn about web assembly and Rust in which to write the logic.

The project was build using vanilla javascript and css for the web elements and interactive logic, and Rust compiled to web assembly for the image processing. The project was kickstarted with [rustwasm/wasm-pack](https://github.com/rustwasm/wasm-pack) which provides a very simple template and compilation tool for working with web assembly and javascript. Using wasm-pack made it easy to get started.
<br><br>

## How it was built
The first step was to create a javascript subproject using `npm init` in a folder that I named `www`. I then built up as simple of a user iterface as possible using just vanilla js and css. This gave me more time to focus on the rust side of the code.

The first challenge was figuring how to get the user selected images from the javascript side to the rust side. This probably took me the better part of a week to figure out and was the single biggest challenge in the project. I went back and forth between using an `src` img element and the html canvas element to access the image information. 

Of course hindsight is 20/20. The solution I used turns out to be pretty obvious in retrospect and was to a method on the JS `FileReader` class which reads the supplied image as an array buffer. This is then accepted by the Rust function that is exposed to JS using `#[wasm-bindgen]`.

In general we want to void copying lots of data back and forth between the two. We want the long lived data to live as Rust structures and Javascript just calls the exposed handles as functions where Rust performs the heavy computation and returns a small, copyable result. Unfortunately, this can't really be the case for this application since we're performing transformations on images that are chosen by the user through the JS interface. This requires copying entire images back and forth.

Thankfully it's not too bad since the images tend to not be very large. In fact, this project could have probably been implemented entirely in JS and performed similarly due to the small scale of the project. But if it were to become a full fledged image editing application designed to work on larger images, the aforementioned best practices would become much more important, so I wanted to mention them.

Once I successfully managed the transfer of the images from JS to Rust, the image processing itself went pretty smoothly. I am using the Rust `Image` library as a convenient way of representing images. Doing this made it much simpler to perform the statistical analysis, which included gathering the standard deviations and means of each channel in both the target and source images, as well as performing per-pixel computations in order transfer the color from one to the other.

I didn't run into too many issue on this step as I'm not doing anything crazy like working on complex types or doing multi-threading (though that would be an interesting topic for future work). 

The final step was to figure out how to get the result of the color transfer inserted into the DOM. I decided to use the `web_sys` crate which allows me to create DOM elements from the Rust side. The decision was somewhat arbitrary in that I could have passed the data back to JS and created the necessary DOM element there. However, in order to get this working, I needed to convert the image data into a format that the DOM `src` tag could understand which is a string with the image data represented as base 64 values. For this I used the `encode` function from the `base64` crate. I also needed to learn about `cursor` from `std::io`.

Then I just append the newly created element to the appropriate section in the DOM and **BLAM!**, works like a charm.
<br><br>

## Lessons Learned
Communication between JS and Rust is limited to passing primitives such as a reference to an array of u8's on the heap. This makes thinking about how to structure your data on both ends important, at least how the communication pipeline should work. You can't just pass a Javascript Object to Rust or a Rust struct back to Javascript and expect to have a good time.

Debugging Rust code from the browser can be tough without the proper setup. Without the proper setup, any runtime error in the Rust code is displayed in the browser log unhelpfully as `Runtime Error: unreachable executed` regardless of the actual error. This is tough to work out since these errors are the ones that cannot be caught by the Rust compiler since they involve data that is passed to it from the Javascript side. I encountered this problem when I was trying to figure out how to get the image data into an appropriate format to pass to Rust.

Thankfully there is a good solution already in the setup provided by `wasm-pack` which is the `init_panic_hook()` function. This just needs to be called once in the Rust code and voila! Good Rust error output in the browser console. It took me awhile to figure this out, but once I did, figuring out problems in sending data from JS to Rust got a lot easier.
<br><br>

## Conclusion
Even though this project was very limited in scope and arguably didn't even benefit from using Rust compiled to WASM for the image processing logic, I am totally sold on doing so and will be using it for any computationally intense task I might have in my web applications in the future. This project was a great introduction to the topic and helped me to get over the initial hurdle of learning how Rust and Javascript cooperate.
