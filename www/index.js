import * as wasm from 'colorific';

let targetImageData;
let sourceImageData;

/////////////////////////
// Target image upload
function showTargetFile() {
  const reader = new FileReader();
  const dataReader = new FileReader();
  const targetImageDisplay = document.getElementById('target-image-display');
  const targetFile = document.getElementById('target-image-input').files[0];

  reader.addEventListener(
    'load',
    () => {
      let targetImage = reader.result;
      targetImageDisplay.src = targetImage;
    },
    false
  );
  dataReader.addEventListener(
    'load',
    () => {
      targetImageData = dataReader.result;
      targetImageData = new Uint8Array(targetImageData);
    },
    false
  );

  if (targetFile) {
    reader.readAsDataURL(targetFile);
    dataReader.readAsArrayBuffer(targetFile);
  }
}

/////////////////////////
// Source image upload
function showSourceFile() {
  const reader = new FileReader();
  const dataReader = new FileReader();
  const sourceImageDisplay = document.getElementById('source-image-display');
  const sourceFile = document.getElementById('source-image-input').files[0];

  reader.addEventListener(
    'load',
    () => {
      let sourceImage = reader.result;
      sourceImageDisplay.src = sourceImage;
    },
    false
  );

  dataReader.addEventListener(
    'load',
    () => {
      sourceImageData = dataReader.result;
      sourceImageData = new Uint8Array(sourceImageData);
    },
    false
  );
  if (sourceFile) {
    reader.readAsDataURL(sourceFile);
    dataReader.readAsArrayBuffer(sourceFile);
  }
}

// Setup event listeners for buttons
document
  .getElementById('source-image-input')
  .addEventListener('change', showSourceFile);
document
  .getElementById('target-image-input')
  .addEventListener('change', showTargetFile);

document.getElementById('transfer-button').addEventListener('click', () => {
  // get data for processing with rust
  let targetImageTransport = new Uint8Array(targetImageData);
  let sourceImageTransport = new Uint8Array(sourceImageData);

  // Call Rust function
  wasm.transfer_color(targetImageTransport, sourceImageTransport);
});
